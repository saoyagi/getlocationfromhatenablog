#coding: utf-8
require 'open-uri'

class BlogParser

  def self.parse(url)
    doc = Nokogiri::HTML(open(url))

    text = ""
    doc.css('div.entry-content').each do |entry|
      text +=  entry.content
    end

    return text
  end
end