# coding: utf-8

require "MeCab"


class PlaceWord

  #テキストを受け取って地名を抽出する
  def self.parse(text)

    retryCount = 0
    begin
      node = MeCab::Tagger.new.parseToNode(text)
    rescue
      retryCount += 1
      if retryCount < 3  #3回はチャレンジする
        retry
      else
        return nil
      end
    end

    result = []

    while node do

      #mecabフォーマット 表層形\t品詞,品詞細分類1,品詞細分類2,品詞細分類3,活用形,活用型,原形,読み,発音
      features = node.feature.force_encoding("UTF-8").split(",")
      #原型
      original = features[6]

      #2番目
      second = features[1]

      #3番目
      third = features[2]

      #表層
      surface = node.surface.force_encoding("UTF-8");

      #地名だったら
      if second== "固有名詞" && third == "地域"
        #対象とする品詞だったら追加
        #words.push({:text => node.surface.force_encoding("UTF-8"), :class =>  apart})
        result.push(original)
      end
      node = node.next
    end

    return result
  end

  #json形式で結果を取り出す
  def self.getJSON(text)
    result = parse(text)

    return JSON.parse(result)
  end


end